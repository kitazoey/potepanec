class Potepan::ProductsController < ApplicationController
  MAX_LENGTH_RELATED_PRODUCTS = 4
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products(MAX_LENGTH_RELATED_PRODUCTS)
    @variants = @product.variants.includes(:option_values)
  end
end
