class Potepan::CartsController < ApplicationController
  protect_from_forgery
  before_action :store_guest_token, except: [:destroy]
  include Spree::Core::ControllerHelpers::Order
  include Spree::Core::ControllerHelpers::Auth
  include Spree::Core::ControllerHelpers::Store

  def index
    @order = Spree::Order.incomplete.find_or_initialize_by(guest_token: cookies.signed[:guest_token])
  end

  def create
    @order = current_order(create_order_if_necessary: true)
    authorize! :update, @order, cookies.signed[:guest_token]

    variant  = Spree::Variant.find(params[:variant_id])
    quantity = params[:quantity].present? ? params[:quantity].to_i : 1

    if !quantity.between?(1, 2_147_483_647)
      @order.errors.add(:base, t('spree.please_enter_reasonable_quantity'))
    end

    begin
      @line_item = @order.contents.add(variant, quantity)
    rescue ActiveRecord::RecordInvalid => error
      @order.errors.add(:base, error.record.errors.full_messages.join(", "))
    end

    if @order.errors.any?
      flash[:error] = @order.errors.full_messages.join(", ")
      redirect_to potepan_carts_path
    else
      redirect_to potepan_carts_path
    end
  end

  def update
    @order = Spree::Order.find(params[:id])
    @order.contents.update_cart(order_params)
    redirect_to potepan_carts_path
  end

  def destroy
    @line_item = Spree::LineItem.find(params[:id])
    @line_item.order.contents.remove_line_item(@line_item)
    redirect_to potepan_carts_path
  end

  private

  def store_guest_token
    cookies.permanent.signed[:guest_token] = params[:token] if params[:token]
  end

  def order_params
    params.require(:order).permit(line_items_attributes: [:id, :quantity, :price])
  end
end
