class Potepan::CategoriesController < ApplicationController
  helper_method :count_option_value_products

  def show
    @taxon = Spree::Taxon.find(params[:id])
    @taxonomies = Spree::Taxonomy.includes(root: :children)
    @color_categories = Spree::OptionType.find_by(presentation: "Color").option_values
    @size_categories = Spree::OptionType.find_by(presentation: "Size").option_values
    @products = @taxon.all_products.includes(master: [:default_price, :images]).
      where.not("available_on > :available_on", { available_on: Time.current }).
      filter_by_option_value(params[:color]).
      filter_by_option_value(params[:size]).
      with_sort_product(params[:sort])
  end

  def count_option_value_products(option_value)
    @taxon.all_products.includes(variants: :option_values).where(spree_option_values: { name: option_value }).count
  end
end
