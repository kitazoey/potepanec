module Spree::ProductDecorator
  def self.prepended(base)
    base.class_eval do
      scope :filter_by_option_value, -> (option_value) {
        return if option_value.blank?
        joins(variants: :option_values).where(spree_option_values: { name: option_value }).distinct
      }

      scope :with_sort_product, -> (sort) {
        if sort == "NEW_PRODUCTS"
          order(available_on: :desc)
        elsif sort == "OLD_PRODUCTS"
          order(available_on: :asc)
        elsif sort == "HIGH_PRICE"
          descend_by_master_price
        elsif sort == "LOW_PRICE"
          ascend_by_master_price
        end
      }

      scope :with_new_release_products, -> (max_length) {
        available.includes(master: [:default_price, :images]).order(available_on: "DESC").limit(max_length)
      }
    end
  end

  def related_products(max_length)
    Spree::Product.in_taxons(taxons).
      includes(master: [:images, :default_price]).
      where.not(id: id).distinct.sample(max_length)
  end

  Spree::Product.prepend self
end
