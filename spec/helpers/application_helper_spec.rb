require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_title" do
    it "returns base_title" do
      expect(helper.full_title).to eq("BIGBAG Store")
    end
    it "return product title" do
      expect(helper.full_title('Ruby')).to eq("Ruby - BIGBAG Store")
    end
  end

  describe "url_with_params" do
    it "return ?color=Red" do
      expect(helper.url_with_params("https://localhost", { color: "Red" })).to eq("https://localhost?color=Red")
    end
  end
end
