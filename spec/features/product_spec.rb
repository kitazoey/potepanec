require 'rails_helper'

RSpec.feature "Products", type: :feature do
  describe "visit products page" do
    let(:taxon) { create(:taxon, name: "Shirts") }
    let!(:product) { create(:product, name: 'Superman T-Shirt', taxons: [taxon]) }
    let!(:related_product) { create(:product, name: 'Woman Shirt', taxons: [taxon]) }
    let!(:not_related_product) { create(:product, name: 'Woman Bag') }

    before do
      visit potepan_product_path(product.id)
    end

    context "when the product exists " do
      it "display product" do
        expect(page).to have_selector 'h2', text: product.name
        expect(page).to have_selector 'h3', text: product.display_price
        expect(page).to have_selector 'p', text: product.description
      end

      it "link to category page" do
        within '.media-body' do
          expect(page).to have_link '一覧ページへ戻る', href: potepan_category_path(taxon.id)
        end
      end
    end

    context "When related_products exist" do
      it "display related_product " do
        within '.productCaption' do
          expect(page).to have_selector 'h5', text: related_product.name
          expect(page).to have_selector 'h3', text: related_product.display_price
        end
        expect(page).to have_link related_product.name, href: potepan_product_path(related_product.id)
      end

      it "dosen't display not_related_product" do
        within '.productCaption' do
          expect(page).not_to have_selector 'h5', text: not_related_product.name
        end
      end
    end
  end
end
