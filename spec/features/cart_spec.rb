require 'rails_helper'

RSpec.feature "Carts", type: :feature do
  describe "Visit carts page" do
    context "when @order is nil" do
      it "display the cart is empty" do
        visit potepan_carts_path
        within ".cartListInner" do
          expect(page).to have_selector 'p', text: "カートは空です。"
        end
      end
    end
  end
end
