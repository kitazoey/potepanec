require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  describe "visit categories page" do
    let!(:taxonomy) { create(:taxonomy, name: 'Category') }
    let!(:taxon1) { taxonomy.root.children.create(name: 'Bags') }
    let!(:taxon2) { taxonomy.root.children.create(name: 'Shirts') }
    let!(:product1) { create(:product, name: 'Tote', taxons: [taxon1]) }
    let!(:product2) { create(:product, name: 'Superman T-Shirts', taxons: [taxon2]) }
    let!(:option_type_color) { create(:option_type, name: "Color", presentation: "Color") }
    let!(:option_type_size) { create(:option_type, name: "Size", presentation: "Size") }
    let!(:option_value_red) { create(:option_value, name: "Red", presentation: "Red", option_type: option_type_color) }
    let!(:option_value_small) { create(:option_value, name: "Small", presentation: "S", option_type: option_type_size) }
    let!(:variant) { product1.variants.create!(price: 5.59) }

    before do
      visit potepan_category_path(taxon1.id)
    end

    it "display products" do
      within '.productCaption' do
        expect(page).to have_selector 'h5', text: product1.name
      end
      expect(page).to have_link product1.name, href: potepan_product_path(product1.id)
      expect(page).to have_selector 'h3', text: product1.display_price
    end

    it "link to taxon2 category page" do
      expect(page).not_to have_selector 'h5', text: product2.name
      within '.collapseItem' do
        click_link taxon2.name
      end
      expect(page).to have_title("Shirts - BIGBAG Store")
      expect(page).not_to have_selector 'h5', text: product1.name
      expect(page).to have_selector 'h5', text: product2.name
    end

    context "When products filter by option_values" do
      let!(:not_option_value_product) { create(:product, name: 'Rick suck', taxons: [taxon1]) }

      before do
        visit potepan_category_path(taxon1.id)
        product1.option_types << option_type_size
        product1.option_types << option_type_color
        variant.option_values << option_value_red
        variant.option_values << option_value_small
      end

      it "is filter by color" do
        click_link option_value_red.presentation
        within '.productCaption' do
          expect(page).to have_selector 'h5', text: product1.name
          expect(page).not_to have_selector 'h5', text: not_option_value_product.name
        end
      end

      it "is filter by Size" do
        within all('.list-unstyled')[1] do
          click_link option_value_small.presentation
        end
        within '.productCaption' do
          expect(page).to have_selector 'h5', text: product1.name
          expect(page).not_to have_selector 'h5', text: not_option_value_product.name
        end
      end
    end
  end
end
