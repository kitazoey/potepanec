require 'rails_helper'

RSpec.feature "Index", type: :feature do
  describe "index page" do
    let!(:new_product) { create(:product, available_on: "2020-02-17 11:11:11") }
    let!(:old_product) { create(:product, available_on: nil) }

    before do
      visit potepan_path
    end

    it "display new products" do
      within '.productCaption' do
        expect(page).to have_selector 'h5', text: new_product.name
        expect(page).to have_selector 'h3', text: new_product.display_price
      end
      expect(page).to have_link new_product.name, href: potepan_product_path(new_product.id)
    end

    it "not display old products" do
      within '.productCaption' do
        expect(page).not_to have_selector 'h5', text: old_product.name
      end
    end
  end
end
