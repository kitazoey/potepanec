require 'rails_helper'

RSpec.describe Spree::ProductDecorator, type: :model do
  describe "#related_products" do
    let(:taxon) { create(:taxon, name: "Shirts") }
    let!(:product) { create(:product, name: 'Superman T-Shirt', taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    it "is gets only 4 related_products" do
      expect(product.related_products(4).count).to eq 4
    end
  end

  describe "#filter_by_option_value" do
    let(:taxon) { create(:taxon, name: "Shirts") }
    let!(:product) { create(:product, name: 'Superman T-Shirt', taxons: [taxon]) }
    let!(:option_type_color) { create(:option_type, name: "Color", presentation: "Color") }
    let!(:option_type_size) { create(:option_type, name: "Size", presentation: "Size") }
    let!(:variant) { product.variants.create!(price: 5.59) }
    let!(:option_value_red) { create(:option_value, name: "Red", presentation: "Red", option_type: option_type_color) }
    let!(:not_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      product.option_types << option_type_color
      variant.option_values << option_value_red
    end

    it "is gets only Red products" do
      expect(Spree::Product.filter_by_option_value(option_value_red.name)).to contain_exactly product
    end
  end

  describe "#with_sort_product" do
    let(:taxon) { create(:taxon, name: "Shirts") }
    let!(:old_product) { create(:product, id: 1, taxons: [taxon], available_on: Time.current.ago(3.years)) }
    let!(:new_product) { create(:product, id: 2, taxons: [taxon], available_on: Time.current) }
    let!(:low_price_product) { create(:product, id: 3, taxons: [taxon], price: 9.5) }
    let!(:high_price_product) { create(:product, id: 4, taxons: [taxon], price: 22.0) }

    it "is sort available_on desc" do
      expect(Spree::Product.with_sort_product("NEW_PRODUCTS").first).to eq new_product
    end

    it "is sort available_on asc" do
      expect(Spree::Product.with_sort_product("OLD_PRODUCTS").first).to eq old_product
    end

    it "is sort price desc" do
      expect(Spree::Product.with_sort_product("HIGH_PRICE").first).to eq high_price_product
    end

    it "is sort price asc" do
      expect(Spree::Product.with_sort_product("LOW_PRICE").first).to eq low_price_product
    end
  end

  describe "#with_new_release_products" do
    let(:taxon) { create(:taxon, name: "Shirts") }
    let!(:old_product) { create(:product, id: 1, taxons: [taxon], available_on: Time.current.ago(3.years)) }
    let!(:new_product) { create(:product, id: 2, taxons: [taxon], available_on: Time.current) }
    let!(:not_release_product) { create(:product, id: 3, taxons: [taxon], available_on: Time.current.since(4.years)) }
    let!(:products) { create_list(:product, 5, taxons: [taxon]) }

    it "is get new release products" do
      expect(Spree::Product.with_new_release_products(6).count).to eq 6
    end
  end
end
