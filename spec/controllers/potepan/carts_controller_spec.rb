require 'rails_helper'
RSpec.describe Potepan::CartsController, type: :controller do
  let!(:store) { create(:store) }
  let!(:product) { create(:product, master: variant) }
  let!(:variant) { create(:variant) }

  context "Get #index" do
    let(:guest_token) { 'abcdefg' }
    let!(:order) { create(:order_with_line_items, completed_at: nil, guest_token: guest_token, store: store) }

    before do
      cookies.signed[:guest_token] = guest_token
      get :index
    end

    it "response product detail page" do
      expect(response).to have_http_status(:ok)
    end

    it "renders the :show template" do
      expect(response).to render_template :index
    end

    it "assign @order" do
      expect(assigns(:order)).to eq order
    end
  end

  context "Patch #update" do
    let(:guest_token) { 'abcdefg' }
    let!(:order) { create(:order_with_line_items, completed_at: nil, guest_token: guest_token, store: store) }
    let(:line_item) { order.contents.add(variant, 1) }

    it "update quantity" do
      expect(line_item.quantity).to eq 1
      put :update, params: { id: order.id, order: { line_items_attributes: { id: line_item.id, quantity: 2 } } }
      expect(line_item.reload.quantity).to eq 2
    end
  end

  context "Post #create" do
    it "create new order" do
      post :create, params: { variant_id: variant.id, quantity: 1 }
      expect(response).to be_redirect
      order_by_token = Spree::Order.find_by(guest_token: cookies.signed[:guest_token])
      assigned_order = assigns[:order]
      expect(assigned_order).to eq order_by_token
      expect(assigned_order).to be_persisted
    end
  end

  context "Delete #destroy" do
    let(:guest_token) { 'abcdefg' }
    let!(:order) { create(:order_with_line_items, completed_at: nil, guest_token: guest_token, store: store) }
    let(:line_item) { order.contents.add(variant, 1) }

    it "remove line_item" do
      expect(order.line_items).to include line_item
      delete :destroy, params: { id: line_item.id }
      expect(order.reload.line_items).not_to include line_item
    end
  end
end
