require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "Get categories" do
    let(:taxonomy) { create(:taxonomy, name: "Category") }
    let(:taxon) { create(:taxon, name: "T_Shirts", taxonomy: taxonomy) }
    let(:product) { create(:product, name: 'Superman T-Shirt', taxons: [taxon]) }
    let!(:option_type_color) { create(:option_type, name: "Color", presentation: "Color") }
    let!(:option_type_size) { create(:option_type, name: "Size", presentation: "Size") }

    before do
      get :show, params: { id: taxon.id }
    end

    describe "Get /categoies/taxon.id" do
      it "renders the category page" do
        expect(response).to have_http_status(:ok)
      end

      it "renders the :show template" do
        expect(response).to render_template :show
      end

      it "assign @taxon" do
        expect(assigns(:taxon)).to eq taxon
      end

      it "assign @products" do
        expect(assigns(:products)).to contain_exactly product
      end

      it "assign @taxonomies" do
        expect(assigns(:taxonomies)).to contain_exactly taxonomy
      end
    end

    describe "Get /categories/taxon.id?color=Red" do
      let!(:variant) { product.variants.create!(price: 5.59) }
      let!(:option_value_red) { create(:option_value, name: "Red", presentation: "Red", option_type: option_type_color) }

      before do
        get :show, params: { id: taxon.id, color: option_value_red.name }
        product.option_types << option_type_color
        variant.option_values << option_value_red
      end

      it "assign @color_categories" do
        expect(assigns(:color_categories)).to contain_exactly option_value_red
      end

      it "return http response" do
        expect(response).to have_http_status(:ok)
      end

      it "assign @products filter by color" do
        expect(assigns(:products)).to contain_exactly product
      end
    end

    describe "Get /categories/taxon.id?Size=Small" do
      let!(:variant) { product.variants.create!(price: 5.59) }
      let!(:option_value_small) { create(:option_value, name: "Small", presentation: "S", option_type: option_type_size) }

      before do
        get :show, params: { id: taxon.id, size: option_value_small.name }
        product.option_types << option_type_size
        variant.option_values << option_value_small
      end

      it "assign @size_categories" do
        expect(assigns(:size_categories)).to contain_exactly option_value_small
      end

      it "return http response" do
        expect(response).to have_http_status(:ok)
      end

      it "assign @products filter by size" do
        expect(assigns(:products)).to contain_exactly product
      end
    end
  end
end
