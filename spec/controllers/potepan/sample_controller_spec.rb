require 'rails_helper'

RSpec.describe Potepan::SampleController, type: :controller do
  describe "Get index" do
    let!(:new_product) { create(:product, available_on: "2020-01-11 11:11:11") }
    let!(:new_products) { create_list(:product, 6, available_on: "2020-02-17 12:00:00") }

    before do
      get :index
    end

    it "response index page" do
      expect(response).to have_http_status(:ok)
    end

    it "renders the :index template" do
      expect(response).to render_template :index
    end

    it "assigns @products" do
      expect(assigns(:products)).to match_array new_products
    end

    it "assigns @products ordered" do
      expect(assigns(:products)).not_to match_array new_product
    end

    it "assigns @products size" do
      expect(assigns(:products).size).to eq 6
    end
  end
end
